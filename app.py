from flask import Flask, jsonify
from flask_cors import CORS
import pymysql
from flask import request
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
CORS(app) # This will enable CORS for all routes

def get_db_connection():
    try:
        connection = pymysql.connect(
            host=os.environ.get('DB_HOST', 'localhost'),
            user=os.environ.get('DB_USER', 'root'),
            password=os.environ.get('DB_PASSWORD', '0000'),
            port=int(os.environ.get('DB_PORT', 3306)),
            db=os.environ.get('DB_NAME', 'mydb')
        )
        return connection
    except pymysql.MySQLError as e:
        app.logger.error(f"Error connecting to MySQL: {e}")
        return None

def fetch_data(id):
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM card_table WHERE password = %s"
            cursor.execute(sql, (id,))
            result = cursor.fetchall()
            return result
    finally:
        connection.close()

@app.route('/')
def home():
    return "Hello, World!"

@app.route('/api/data/<int:id>', methods=['GET'])
def get_data(id):
    print(id)
    data = fetch_data(id)
    print(data)
    return jsonify(data)

@app.route('/api/data/<int:id>', methods=['DELETE'])
def delete_data(id):
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            sql = "DELETE FROM card_table WHERE password = %s"
            cursor.execute(sql, (id,))
            connection.commit()
            return jsonify({"message": "Record deleted successfully"}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    finally:
        connection.close()

@app.route('/api/data/<int:id>', methods=['PUT'])
def update_data(id):
    data = request.get_json()
    print("Received data:", data)
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            sql = "UPDATE card_table SET name = %s, attribute = %s, level = %s, atk = %s, def = %s, url = %s, price = %s, remain = %s WHERE password = %s"
            cursor.execute(sql, (data['name'], data['attribute'], data['level'], data['atk'], data['def'], data['url'], data['price'], data['remain'], id))
            connection.commit()
            return jsonify({"message": "Record updated successfully"}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    finally:
        connection.close()

@app.route('/api/data', methods=['POST'])
def create_data():
    data = request.get_json()
    # print("Received data:", data)
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            # Assuming your table has columns 'name' and 'description'
            # Adjust the SQL query and parameters based on your actual table structure
            sql = "INSERT INTO card_table (password, name, attribute, level, atk, def, url, price, remain) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(sql, (data['password'], data['name'], data['attribute'], data['level'], data['atk'], data['def'], data['url'], data['price'], data['remain']))
            connection.commit()
            return jsonify({"message": "Record created successfully"}), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    finally:
        connection.close()


# auth part
@app.route('/api/login', methods=['POST'])
def login():
    data = request.get_json()
    username = data.get('username')
    password = data.get('password')
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM users WHERE username = %s AND password = %s"
            cursor.execute(sql, (username, password))
            result = cursor.fetchone()
            if result:
                return jsonify({"message": "Login successful"}), 200
            else:
                return jsonify({"error": "Invalid credentials"}), 401
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    finally:
        connection.close()

@app.route('/api/signup', methods=['POST'])
def create_userdata():
    data = request.get_json()
    # print("Received data:", data)
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            # Assuming your table has columns 'name' and 'description'
            # Adjust the SQL query and parameters based on your actual table structure
            sql = "INSERT INTO users (id, username, email, password) VALUES (%s, %s, %s, %s)"
            cursor.execute(sql, (data['id'], data['username'], data['email'], data['password']))
            connection.commit()
            return jsonify({"message": "Record created successfully"}), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    finally:
        connection.close()

def fetch_userdata(id):
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM users WHERE username = %s"
            cursor.execute(sql, (id,))
            result = cursor.fetchall()
            return result
    finally:
        connection.close()

@app.route('/api/user/<string:id>', methods=['GET'])
def get_userdata(id):
    data = fetch_userdata(id)
    return jsonify(data)

# cart part
@app.route('/cart', methods=['POST'])
def update_cart():
    print("Updating!!!")
    data = request.get_json()
    username = data.get('username')
    password = data.get('password')
    number = data.get('number')
    remain = data.get('remain')
    price = data.get('price')
    print(data)
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM cart WHERE username = %s and password = %s"
            cursor.execute(sql, (data['username'], data['password'],))
            results = cursor.fetchall()
            if results:
                # update the cart
                number+=results[0][3]
                if number > remain:
                    number = remain
                price = number * price
                sql = "UPDATE cart SET numbers = %s, money = %s WHERE idcart = %s"
                cursor.execute(sql, ( number, price, results[0][0]))
                connection.commit()
                return jsonify({"message": "Update Record created successfully"}), 201
            else:
                # create the cart
                if number>remain:
                    number = remain
                price = number * price
                sql = "INSERT INTO cart (username, password, numbers, money) VALUES (%s, %s, %s, %s)"
                cursor.execute(sql, (data['username'], data['password'], number, price))
                connection.commit()
                return jsonify({"message": "Create Record created successfully"}), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    finally:
        connection.close()

def fetch_cart_data(id):
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM cart WHERE username = %s"
            cursor.execute(sql, (id,))
            result = cursor.fetchall()
            return result
    finally:
        connection.close()


@app.route('/cart/<string:id>', methods=['GET'])
def get_cart_data(id):
    data = fetch_cart_data(id)
    return jsonify(data)

@app.route('/cart/<int:id>', methods=['DELETE'])
def delete_cart_data(id):
    connection = get_db_connection()
    print(id)
    try:
        with connection.cursor() as cursor:
            sql = "DELETE FROM cart WHERE idcart = %s"
            cursor.execute(sql, (id,))
            connection.commit()
            return jsonify({"message": "Record deleted successfully"}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    finally:
        connection.close()

@app.route('/cart/<string:id>', methods=['DELETE'])
def delete_whole_cart_data(id):
    connection = get_db_connection()
    print(id)
    try:
        with connection.cursor() as cursor:
            sql = "DELETE FROM cart WHERE username = %s"
            cursor.execute(sql, (id,))
            connection.commit()
            return jsonify({"message": "Record deleted successfully"}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    finally:
        connection.close()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.environ.get('API_PORT', 5000)))
